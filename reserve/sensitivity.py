import csv
from dataclasses import dataclass
import io
import numpy as np
import reserve
import result
from SALib.sample import sobol as sobol_sample
from SALib.analyze import sobol as sobol_analyze
from SALib.sample.morris import sample as morris_sample
from SALib.analyze.morris import analyze as morris_analyze
from typing import List


@dataclass
class UncertainMInParam:
    inflation_rate: (float, float)  # annual inflation rate (r), low and high


@dataclass
class UncertainComponent:
    name: str
    ul: int  # useful life, in years
    rul: (int, int)  # remaining useful life in year zero, in years
    cost: (float, float)  # replacement cost, in USD, low and high

    def to_certain(self, kind: str) -> reserve.Component:
        """
        :param kind: ml | worst | best
        :return: a Component based on this UncertainComponent
        """
        match kind:
            case 'ml':
                half_rul = round((self.rul[0] + self.rul[1]) / 2.0)
                half_cost = (self.cost[0] + self.cost[1]) / 2.0
                return reserve.Component(self.name, self.ul, half_rul, half_cost)
            case 'best':
                return reserve.Component(self.name, self.ul, self.rul[1], self.cost[0])
            case 'worst':
                return reserve.Component(self.name, self.ul, self.rul[0], self.cost[1])
            case _:
                raise ValueError(f'kind {kind} is not defined in UncertainComponent.to_certain()')


@dataclass
class UncertainMIn:
    components: List[UncertainComponent]


@dataclass
class SobolMOut:
    sobol: dict
    runs: List[reserve.MOut]
    problem: dict


@dataclass
class MorrisMOut:
    morris: dict  # https://salib.readthedocs.io/en/latest/api.html#method-of-morris
    problem: dict


def _evaluate_expense_model(udin: UncertainMIn, X: np.ndarray) -> reserve.MOut:
    """
    Run an expense reserve study using the X params and return the sum of the expenses.
    These studies ignore balances and interest rate.

    The X array consists of the first element as the sample inflation rate (r), followed
    by pairs of rul and cost sample value for each component.

    :param udin: model inputs
    :param X: see above
    :return:
    """
    params = reserve.MInParam(0.0, X[0], 0.0)

    components = []
    for i, comp in enumerate(udin.components):
        components.append(reserve.Component(comp.name, comp.ul, round(X[2*i + 1]), X[2*i + 2]))
    din = reserve.MIn(components, 0.0)

    return reserve.run_simulation(din, params)


def _bounds(bound: (float, float)) -> (float, float):
    """
    Given a pair of bounds (a low and high), if low == high, return (low, low + eps). Otherwise,
    return (low, high)

    eps is a very small number.

    :param bound: pair of numbers
    :return: pair of numbers
    """
    # SALib checks that all bounds have a lower bound < higher bound. Since a bound with no variance will not
    # contribute any variance to the output, this is keeping out unnecessary variables. However, that is an
    # inconvenient restriction for our use case where a community association may have components with well-defined
    # remaining life or known costs. So they can enter the same number as the low and high bounds, we adjust
    # the high bound by a very small amount.
    eps = 0.000_000_001

    if bound[0] == bound[1]:
        return (bound[0], bound[0] + eps)
    else:
        return bound


def _problem(din: UncertainMIn, params: UncertainMInParam) -> dict:
    """
    Generate a SALib-compatible 'problem' dictionary

    :param din: model data input
    :param params: model parameters
    :return:
    """
    names = []
    bounds = []
    names.append('r')  # inflation rate
    bounds.append(_bounds(params.inflation_rate))

    for c in din.components:
        names.append(c.name + '_rul')
        names.append(c.name + '_cost')
        bounds.append(_bounds(c.rul))
        bounds.append(_bounds(c.cost))

    assert len(names) == len(bounds)

    return {
        'num_vars': len(names),
        'names': names,
        'bounds': bounds
    }


def run_expense_sensitivity_analysis(din: UncertainMIn, params: UncertainMInParam, n: int = 1024) -> SobolMOut:
    """
    Run a Sobol-type sensitivity analysis to determine the contribution of variance of the inputs
    to the output variance (sum of all expense costs).

    :param din: model data input
    :param params: model parameters
    :param n: number of samples to generate
    :return: a Si-style dictionary (https://salib.readthedocs.io/en/latest/user_guide/basics.html#perform-analysis)
    """
    problem = _problem(din, params)

    param_values = sobol_sample.sample(problem, n)

    Y = np.zeros([param_values.shape[0]])
    runs = []

    for i, X in enumerate(param_values):
        mout = _evaluate_expense_model(din, X)
        runs.append(mout)
        Y[i] = mout.expense_by_year.sum()

    Si = sobol_analyze.analyze(problem, Y)

    return SobolMOut(Si, runs, problem)


def run_expense_ranking_analysis(din: UncertainMIn, params: UncertainMInParam, n: int = 1000) -> MorrisMOut:
    """
    Run a Morris-style sensitivity analysis to determine the contribution to expenses of the various factors.

    :param din: model data input
    :param params: model parameters
    :param n: number of trajectories to generate
    :return: a MorrisMOut instance
    """
    problem = _problem(din, params)

    Xs: np.ndarray = morris_sample(problem, n)

    Y = np.zeros([Xs.shape[0]])
    runs = []

    for i in range(0, Xs.shape[0]):
        mout = _evaluate_expense_model(din, Xs[i])
        runs.append(mout)
        Y[i] = mout.expense_by_year.sum()

    Si = morris_analyze(problem, Xs, Y)
    return MorrisMOut(Si, problem)


def components_from_csv(data: str) -> result.Result[List[UncertainComponent], str]:
    """
    From a string (maybe) containing a component model in a comma-separated value format,
    return Ok(List(UncertainComponent) or an Err with a ValueError.

    :param data: a string purportedly containing a CSV-formatted component model
    :return: OK of a list of UncertainComponent or Err with an error message
    """
    mandatory_headers = ['Component', 'UL']
    expected_alternates = [(['Cost'], ['Cost_Low', 'Cost_High']), (['RUL'], ['RUL_Low', 'RUL_High'])]

    try:
        dialect = csv.Sniffer().sniff(data)
    except csv.Error:
        # contrary to the Python documentation, sniff() returns a dialect OR raises a csv.Error exception
        print(f'Submitted CSV file contained {len(data.splitlines())} lines across {len(data)} characters')
        return result.Err('Unable to determine dialect of CSV file. Is input in CSV format?')

    components: List[UncertainComponent] = []

    reader = csv.DictReader(io.StringIO(data), dialect=dialect)
    rows: List[(int, str)] = []
    for row in reader:
        rows.append((reader.line_num, row))

    # verify we can pull from expected keys
    found = set(reader.fieldnames)
    if not set(mandatory_headers).issubset(found):
        not_avail = ', '.join(set(mandatory_headers).difference(found))
        return result.Err(f'Expected headers {not_avail} were not found')
    for (a, b) in expected_alternates:
        if not set(a).issubset(found) and not set(b).issubset(found):
            hd = ', '.join(a) + ' or ' + ', '.join(b)
            return result.Err(f'Expected either {hd} but neither header were found')

    for (line_num, row) in rows:
        try:
            name = row['Component']
            ul = int(row['UL'])

            if 'RUL_Low' in row:
                rul_low = int(row['RUL_Low'])
                rul_high = int(row['RUL_High'])
            else:
                rul_low = int(row['RUL'])
                rul_high = rul_low

            if 'Cost_Low' in row:
                cost_low = float(row['Cost_Low'])
                cost_high = float(row['Cost_High'])
            else:
                cost_low = float(row['Cost'])
                cost_high = cost_low

            components.append(UncertainComponent(name, ul, (rul_low, rul_high), (cost_low, cost_high)))
        except ValueError as e:
            return result.Err(f'Value could not be understood on line {line_num}: {e}')

    return result.Ok(components)


MAX_COMPONENTS_FOR_CDF = 12


def ffb_cdf(components: List[reserve.Component]) -> result.Result[np.ndarray, str]:
    """
    Return a matrix with 2**|components| rows and two columns. The first column (x) represents the cumulative weighted
    cost or magnitude of a certain set of components needing replacement. The second column (y) is the cumulative
    probability of that event. The matrix will be sorted by y values in descending order.

    If |components| > MAX_COMPONENTS_FOR_CDF, return result.Err(
    'Too many components for FFB CDF analysis. Saw n, limit is N.')

    :param components: component inventory
    :return: Result of data for a CDF plot of FFB values or Err(message)
    """
    if len(components) > MAX_COMPONENTS_FOR_CDF:
        return result.Err(f'Too many components for FFB CDF analysis. Saw {len(components)}, limit is {MAX_COMPONENTS_FOR_CDF}.')
    if len(components) == 0:
        return result.Ok(np.array([[1.0, 0]]))

    magnitudes: np.ndarray = np.empty(2**len(components))
    probs: np.ndarray = np.empty(magnitudes.shape)

    for mask in range(0, magnitudes.shape[0]):
        ps = np.empty(len(components))
        mags = np.empty(len(components))

        for i, c in enumerate(components):
            if mask & 1 << i:
                ps[i] = c.effective_percent_age()
                mags[i] = c.cost
            else:
                ps[i] = 1.0 - c.effective_percent_age()
                mags[i] = 0.0

        prob = np.prod(ps)
        w_m = np.sum(mags) * prob
        magnitudes[mask] = w_m
        probs[mask] = prob

    matrix = []
    for m, p in zip(magnitudes, probs):
        matrix.append([m, p])

    matrix.sort(key=lambda m: m[1], reverse=True)

    cdf = np.array(matrix)
    cdf = np.cumsum(cdf, axis=0)

    return result.Ok(cdf)
