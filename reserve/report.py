import base64
from io import BytesIO
from jinja2 import Environment, FileSystemLoader, select_autoescape
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from matplotlib.ticker import FuncFormatter
import numpy as np
import reserve
import reserve.sensitivity
from result import Ok, Err
from typing import List


def _new_figure() -> Figure:
    """
    :return: a new Figure configured with defaults
    """
    return Figure(figsize=(7.2, 4.8), dpi=100)


def _render_svg(figure: Figure) -> str:
    """
    :param figure: populated figure ready for rendering
    :return: html embedded image with a base64 encoded svg image
    """
    buf = BytesIO()
    figure.savefig(buf, format='svg')
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    # Potential improvement: Compress the image
    return f'<img src="data:image/svg+xml;base64,{data}"/>'


def _thousands(x, pos):
    """
    Return the tick formatted in thousands
    :param x: tick value
    :param pos: position
    :return: 5400 -> 5.4 or 10000 -> 10.0 (or similar)
    """
    return '%1.1f' % (x * 1e-3)


def cum_expenses_boxplot(runs: List[reserve.MOut]) -> str:
    """
    Generate an SVG graphic containing a box-and-whisker plot of cumulative expenses per year

    :param runs:
    :return:
    """
    fig = _new_figure()

    ax: Axes = fig.subplots(nrows=1, ncols=1)
    ax.set_title('Cumulative Expenses Per Year Box Plot')
    ax.set_xlabel('Year')
    ax.set_ylabel('Cum. Expense ($k)')
    ax.yaxis.set_major_formatter(FuncFormatter(_thousands))

    expenses = np.zeros((len(runs), reserve.YEARS))
    for i, run in enumerate(runs):
        for j in range(0, reserve.YEARS):
            expenses[i][j] = run.expense_by_year[j][0]
    cum_expenses = np.cumsum(expenses, axis=1)
    ax.boxplot(cum_expenses, showfliers=False)

    return _render_svg(fig)


def expenses_boxplot(runs: List[reserve.MOut]) -> str:
    """
    Generate an SVG graphic containing a box-and-whisker plot of total expenses per year

    :param runs:
    :return: str encoded with Scalable Vector Graphic
    """
    fig = _new_figure()

    ax: Axes = fig.subplots(nrows=1, ncols=1)
    ax.set_title('Total (Sum) Expenses Per Year Box Plot')
    ax.set_xlabel('Year')
    ax.set_ylabel('Expense ($k)')
    ax.yaxis.set_major_formatter(FuncFormatter(_thousands))

    # boxplot expects data as n-rows by years (30) columns; `expense_by_year` is a column vector
    # of 30 rows (year) and 1 column.
    expenses = np.zeros((len(runs), reserve.YEARS))
    for i, run in enumerate(runs):
        for j in range(0, reserve.YEARS):
            expenses[i][j] = run.expense_by_year[j][0]
    ax.boxplot(expenses, showfliers=False)

    return _render_svg(fig)


def sobol_list(mout: reserve.sensitivity.SobolMOut) -> List[str]:
    """
    Export a list of the components that contribute the most to the uncertainty in the output
    :param mout:
    :return:
    """
    total_order: (float, str) = []
    for st, name in zip(mout.sobol['ST'], mout.problem['names']):
        if st > 0.05:
            total_order.append((st, name))

    total_order.sort(reverse=True)

    return [f'{t[1]} ({t[0]:.0%})' for t in total_order]


def morris_list(mout: reserve.sensitivity.MorrisMOut) -> List[str]:
    """
    Export a list of the components that contribute the most to the output
    :param mout:
    :return:
    """
    mu_star = mout.morris['mu_star']
    mu_star_sum = mu_star.sum()

    contribs: (float, str) = []
    for mus, name in zip(mu_star, mout.problem['names']):
        contrib_percent = mus / mu_star_sum
        if contrib_percent >= 0.05:
            contribs.append((contrib_percent, name))

    contribs.sort(reverse=True)

    return [f'{t[1]} ({t[0]:.0%})' for t in contribs]


def ffb_plot(min: reserve.sensitivity.UncertainMIn, mparam: reserve.sensitivity.UncertainMInParam):
    """
    Plot the fully-funded balance based on different 'kind' transformation of uncertain components,
    along with the variability in inflation

    :param min:
    :param mparam:
    :return:
    """
    fig = _new_figure()

    ax: Axes = fig.subplots(nrows=1, ncols=1)
    ax.set_title('Fully Funded Balances per Year')
    ax.set_xlabel('Year')
    ax.set_ylabel('Balance ($k)')
    ax.yaxis.set_major_formatter(FuncFormatter(_thousands))

    for kind in ['ml', 'worst', 'best']:
        mls = [c.to_certain(kind) for c in min.components]
        ml_inflation = (mparam.inflation_rate[0] + mparam.inflation_rate[1]) / 2.0

        Y = np.zeros((reserve.YEARS, len(mls)))
        for j, ml in enumerate(mls):
            for i in range(0, reserve.YEARS):
                Y[i][j] = ml.ffb_projection(i, ml_inflation)

        label = '?kind'
        match kind:
            case 'ml':
                label = 'Mean RUL, Cost, and r'
            case 'best':
                label = 'Best case RUL and Cost, Mean r'
            case 'worst':
                label = 'Worst case RUL and Cost, Mean r'
        ax.plot(range(1, reserve.YEARS+1), Y.sum(axis=1), label=label)

    fig.legend(loc='upper left', bbox_to_anchor=(0.15, 0.85))

    return _render_svg(fig)


def ffb_cashflow_table(min: reserve.sensitivity.UncertainMIn,
                       mparam: reserve.sensitivity.UncertainMInParam,
                       mout: reserve.sensitivity.SobolMOut,
                       horizon: int = 10) -> (np.ndarray, np.ndarray, np.ndarray):
    """
    Fully Funded Balance Cashflow and Contributions

    ffb (mean/median) define the initial balance per year
    ffb initial |
    expenses (mean) |
    contribution |
    ffb final (year+1)

    :param min:
    :param mparam:
    :param mout:
    :return: three 4x10 ndarrays for best case, most likely, and worst case
    """
    FFB_INIT = 0
    EXPENSE = 1
    CONTRIB = 2
    FFB_END = 3

    tables = []
    for form in ('best', 'ml', 'worst'):
        table = np.zeros((4, horizon))

        ml_cs = [c.to_certain(form) for c in min.components]
        match form:
            case 'best':
                ml_inflation = mparam.inflation_rate[0]
            case 'ml':
                ml_inflation = (mparam.inflation_rate[0] + mparam.inflation_rate[1]) / 2.0
            case 'worst':
                ml_inflation = mparam.inflation_rate[1]
            case _:
                raise ValueError('form is unknown')

        ffb: List[float] = []
        for y in range(0, horizon + 1):
            ffb.append(np.sum([ml_c.ffb_projection(y, ml_inflation) for ml_c in ml_cs]))

        expenses: List[float] = []
        for y in range(0, horizon):
            match form:
                case 'best':
                    expenses.append(np.min([r.expense_by_year[y][0] for r in mout.runs]))
                case 'ml':
                    expenses.append(np.median([r.expense_by_year[y][0] for r in mout.runs]))
                case 'worst':
                    expenses.append(np.max([r.expense_by_year[y][0] for r in mout.runs]))
                case _:
                    raise ValueError('form is unknown')

        for y in range(0, horizon):
            table[FFB_INIT][y] = ffb[y]
            table[FFB_END][y] = ffb[y+1]
            table[EXPENSE][y] = expenses[y]
            table[CONTRIB][y] = table[FFB_END][y] - (table[FFB_INIT][y] - table[EXPENSE][y])

        tables.append(table)

    return tuple(tables)


def ffb_cdf_plot(min: reserve.sensitivity.UncertainMIn) -> str:
    """
    Either return an SVG CDF plot of FFB values (for the next year),
    or return a html snippet with an error message.

    :param min:
    :return:
    """
    fig = _new_figure()

    ax: Axes = fig.subplots(nrows=1, ncols=1)
    ax.set_title('CDF of Fully Funded Balances for Next Year')
    ax.set_xlabel('Balance ($k)')
    ax.set_ylabel('Cumulative Probability')
    ax.xaxis.set_major_formatter(FuncFormatter(_thousands))

    for form in ('best', 'ml', 'worst'):
        cs = [c.to_certain(form) for c in min.components]
        maybe_cdf = reserve.sensitivity.ffb_cdf(cs)
        match maybe_cdf:
            case Err(mesg):
                return f'<p class="warn">{mesg}</p>'
            case Ok(cdf):
                label = '?kind'
                match form:
                    case 'ml':
                        label = 'Most Likely'
                    case 'best':
                        label = 'Best Case'
                    case 'worst':
                        label = 'Worst Case'
                ax.plot(cdf[:, 0], cdf[:, 1], label=label)

    fig.legend(loc='lower right', bbox_to_anchor=(0.90, 0.11))

    return _render_svg(fig)


def render_report(min: reserve.sensitivity.UncertainMIn,
                  mparam: reserve.sensitivity.UncertainMInParam,
                  mout: reserve.sensitivity.SobolMOut,
                  morris: reserve.sensitivity.MorrisMOut) -> str:
    """

    :param min:
    :param mparam:
    :param mout:
    :return:
    """
    img_expenses_boxplot = expenses_boxplot(mout.runs)
    img_cum_expenses_boxplot = cum_expenses_boxplot(mout.runs)
    sobol_sensitivity = sobol_list(mout)
    morris_sensitivity = morris_list(morris)
    ffb = ffb_plot(min, mparam)
    ffb_cashflows = ffb_cashflow_table(min, mparam, mout)
    img_ffb_cdf = ffb_cdf_plot(min)

    env = Environment(
        loader=FileSystemLoader("templates"),
        autoescape=select_autoescape()
    )

    return env.get_template('base.html').render(mparam=mparam,
                                                img_expenses_boxplot=img_expenses_boxplot,
                                                img_cum_expenses_boxplot=img_cum_expenses_boxplot,
                                                sobol_sensitivity=sobol_sensitivity,
                                                morris_sensitivity=morris_sensitivity,
                                                img_ffb=ffb,
                                                ffb_cashflows=ffb_cashflows,
                                                img_ffb_cdf=img_ffb_cdf)
