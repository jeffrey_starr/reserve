# Reserve Study (Analysis)

import numpy as np
from dataclasses import dataclass
from math import pow
from typing import List


YEARS = 30  # number of years to capture within the study


@dataclass
class Component:
    name: str
    ul: int  # useful life, in years
    rul: int  # remaining useful life in year zero, in years
    cost: float  # replacement cost, in USD

    def effective_percent_age(self) -> float:
        """
        :return: effective age for this component
        :return:
        """
        eff_age = self.ul - self.rul
        return eff_age / self.ul

    def ffb(self) -> float:
        """
        :return: fully funded balance for this component
        """
        # current cost * effective age / useful life
        eff_age = self.ul - self.rul
        return self.cost * eff_age / float(self.ul)

    def ffb_projection(self, year: int, inflation_rate: float) -> float:
        """
        :param year: year index (so, 0 is year, 1 is year+1, 4 is year+4)
        :param inflation_rate: inflation rate
        :return: fully funded balanced for this component in forward year
        """
        eff_rul = (self.rul - year) % self.ul
        eff_age = self.ul - eff_rul
        inflated_cost = self.cost * pow(1.0 + inflation_rate, year)
        return inflated_cost * eff_age / float(self.ul)

    def replacement_years(self) -> np.ndarray:
        """
        :return: a binary row of YEARS columns where 1 indicates component will need to be replaced in this year
        """
        repls = np.zeros(YEARS, dtype=np.int8)
        for r in range(self.rul, YEARS, self.ul):
            repls[r] = 1
        return repls


@dataclass
class MIn:
    components: List[Component]
    starting_balance: float  # initial reserve balance, in USD

    def ffb(self) -> float:
        """
        :return: fully funded balance (sum of all components)
        """
        return sum([c.ffb() for c in self.components])

    def ffb_projection(self, year: int, inflation_rate: float) -> float:
        """
        :param year: year offset
        :param inflation_rate: inflation rate
        :return: fully funded balance for year (sum of all components)
        """
        return sum([c.ffb_projection(year, inflation_rate) for c in self.components])


@dataclass
class MInParam:
    # if reserve_contrib is a float, inflation will be applied. if ndarray, value will be used directly.
    reserve_contrib: float | np.ndarray  # yearly reserve contribution
    inflation_rate: float  # annual inflation rate
    interest_rate: float  # annual interest rate

    def reserve_contrib_norm(self) -> np.ndarray:
        """
        :return: reserve contributions per year
        """
        if isinstance(self.reserve_contrib, np.ndarray):
            return self.reserve_contrib
        else:
            inflate = np.zeros(YEARS)
            for y in range(0, YEARS):
                inflate[y] = self.reserve_contrib * pow(1.0 + self.inflation_rate, y)
            return inflate


@dataclass
class MOut:
    expense_by_year: np.ndarray  # column vector of YEARS
    end_balance_by_year: np.ndarray  # row vector of YEARS


def _end_balance_by_year(expense_by_year: np.ndarray,
                         starting_balance: float,
                         reserve_contrib: np.ndarray,
                         interest_rate: float) -> np.ndarray:
    """
    Return an array of YEARS with the ending balance of the association. The starting balance of the year has
    the corresponding expense_by_year subtracted and the reserve_contrib added.

    :param expense_by_year: column vector of YEARSx1 containing yearly expenses
    :param starting_balance: initial balance of reserve fund
    :param reserve_contrib: yearly contributions to reserve
    :param interest_rate: interest rate, applied to the starting balance in year+1 and later
    :return:
    """
    end_balances = np.zeros(YEARS, dtype=float)
    end_balances[0] = starting_balance - expense_by_year[0][0] + reserve_contrib[0]

    for i in range(1, YEARS):
        interest = end_balances[i-1] * interest_rate
        end_balances[i] = end_balances[i-1] - expense_by_year[i][0] + reserve_contrib[i] + interest

    return end_balances


def _expenses_inflation_adj(constant_expenses: np.ndarray, inflation_rate: float) -> np.ndarray:
    """
    Apply the inflation_rate to year+1 and later

    If inflation_rate is zero, return the constant_expenses array. Otherwise, return a new array.

    :param constant_expenses: column vector of YEARS
    :param inflation_rate: estimated constant inflation rate
    :return: expenses adjusted from constant dollars to inflation-adjusted dollars
    """
    if inflation_rate == 0.0:
        return constant_expenses
    else:
        inflated = np.empty(constant_expenses.shape)
        inflated[0][0] = constant_expenses[0][0]

        for y in range(1, inflated.shape[0]):
            inflated[y][0] = constant_expenses[y][0] * pow(1.0 + inflation_rate, y)
        return inflated


def run_simulation(din: MIn, dparam: MInParam) -> MOut:
    """
    Over YEARS, simulate expenses as components need maintenance and track expenses and
    the balance of a community association during that time.

    :param din: MIn instance
    :param dparam: MInParam instance
    :return: MOut instance
    """
    # Given a YxC (year by component) matrix indicating when a replacement cost will occur,
    # and a Cx1 (component price) row indicating the price of a replacement,
    # YxC multiplied by Cx1 is the accumulated cost or expense in a single year

    replace_by_year = np.empty((YEARS, len(din.components)))
    for i, comp in enumerate(din.components):
        comp_year = comp.replacement_years()
        for j in range(0, YEARS):
            replace_by_year[j][i] = comp_year[j]

    cost_by_comp = np.zeros((len(din.components), 1), dtype=float)
    for i, comp in enumerate(din.components):
        cost_by_comp[i][0] = comp.cost

    expense_by_year = replace_by_year @ cost_by_comp
    expense_by_year = _expenses_inflation_adj(expense_by_year, dparam.inflation_rate)

    end_balances = _end_balance_by_year(expense_by_year,
                                        din.starting_balance,
                                        dparam.reserve_contrib_norm(),
                                        dparam.interest_rate)

    return MOut(expense_by_year, end_balances)
