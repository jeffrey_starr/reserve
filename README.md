# Reserve Study / Analysis Tool

## Development

### Running Tests

```commandline
python -m unittest tests/*.py
```

To include slow tests:

```commandline
TEST_SLOW=1 python -m unittest tests/*.py
```

### Running Coverage

```commandline
TEST_SLOW=1 coverage run -m unittest tests/*.py
coverage report -m
```
