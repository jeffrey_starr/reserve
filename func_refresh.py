from google.cloud import functions_v2 as functions


def update_function(name: str, bucket: str, object_key: str):
    """

    :param name: function id in the form projects/*/locations/*/functions/*
    :param bucket: cloud storage bucket name
    :param object_key: object key in bucket containing the source code zip file
    :return:
    """
    functions_client = functions.FunctionServiceClient()
    function = functions_client.get_function(name=name)

    print(f'Fetched function definition: {function}')

    updated = {
        'name': name,
        'build_config': {
            'source': {
                'storage_source': {
                    'bucket': bucket,
                    'object_': object_key
                }
            }
        }
    }

    req_update = functions.UpdateFunctionRequest({'function': updated, 'update_mask':
                                                  'buildConfig.source.storageSource.bucket,buildConfig.source.storageSource.object'})
    resp = functions_client.update_function(req_update)

    print(f'Update function response: {resp.result()}')


if __name__ == '__main__':
    update_function('projects/reserve-study/locations/us-west1/functions/do-study',
                    'reserve-deployments',
                    'reserve.zip')
