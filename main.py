import dataclasses

import flask
import functions_framework
import result
from result import Ok, Err
from reserve import sensitivity
from reserve import report
from werkzeug.datastructures import ImmutableMultiDict


def _parse_float(s: str, name: str) -> result.Result[float, str]:
    try:
        return Ok(float(s))
    except ValueError:
        return Err(f'{name} could not be converted to a number')


@dataclasses.dataclass
class CombinedInputs:
    m_in: sensitivity.UncertainMIn
    m_p: sensitivity.UncertainMInParam


def _parse_form(form: ImmutableMultiDict) -> result.Result[CombinedInputs, str]:
    """
    Parse the request form and return either model inputs or a string containing error messages.

    The form is expected to have:
        inflation_rate_low      float           mandatory
        inflation_rate_high     float           mandatory
        component_model         text as CSV     mandatory

    :param form:
    :return:
    """
    maybe_i_rate_low = _parse_float(form['inflation_rate_low'], 'Inflation Rate (Low)')
    maybe_i_rate_high = _parse_float(form['inflation_rate_high'], 'Inflation Rate (High)')
    maybe_component_model = sensitivity.components_from_csv(form['component_model'].strip())

    match (maybe_i_rate_low, maybe_i_rate_high, maybe_component_model):
        case (Ok(low), Ok(high), Ok(model)):
            return Ok(CombinedInputs(sensitivity.UncertainMIn(model),
                                     sensitivity.UncertainMInParam(inflation_rate=(low, high))))
        case _:
            errors = [r.err() for r in [maybe_i_rate_low, maybe_i_rate_high, maybe_component_model] if r.is_err()]
            return Err('\n'.join(errors))


@functions_framework.http
def do_study(request: flask.Request) -> flask.Response:
    if request.method == 'OPTIONS':
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Max-Age': '3600'
        }

        return flask.Response("", status=204, headers=headers)
    elif request.method == 'POST':
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*'
        }
        maybe_inputs = _parse_form(request.form)
        maybe_mout = maybe_inputs.map(lambda c: sensitivity.run_expense_sensitivity_analysis(c.m_in, c.m_p))
        maybe_morris = maybe_inputs.map(lambda c: sensitivity.run_expense_ranking_analysis(c.m_in, c.m_p))
        match maybe_mout, maybe_morris:
            case (Ok(m1), Ok(m2)):
                maybe_html = maybe_mout.map(lambda mo: report.render_report(maybe_inputs.ok().m_in, maybe_inputs.ok().m_p, m1, m2))
            case (Err(m1), _):
                maybe_html = maybe_mout
            case _:
                maybe_html = maybe_morris

        match maybe_html:
            case Ok(html):
                return flask.Response(html, status=200, headers=headers)
            case Err(msgs):
                return flask.Response(msgs, status=400, headers=headers)
    else:
        return flask.Response('Invalid HTTP method', status=405)
