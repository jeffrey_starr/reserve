import base64
import unittest
from os import getenv
from reserve import sensitivity
from reserve.report import expenses_boxplot, render_report
import xml.etree.ElementTree as ET


class TestReport(unittest.TestCase):

    model_in: sensitivity.UncertainMIn = None
    params: sensitivity.UncertainMInParam = None
    sobol_out: sensitivity.SobolMOut = None  # initialized in setupClass
    morris_out: sensitivity.MorrisMOut = None  # initialized in setupClass

    @classmethod
    def setUpClass(cls) -> None:
        with open('data/basic_model_cost_20pct.csv', mode='rt', newline='') as f:
            datum = f.read()

        components = sensitivity.components_from_csv(datum).ok()
        cls.model_in = sensitivity.UncertainMIn(components)
        cls.params = sensitivity.UncertainMInParam(inflation_rate=(0.02, 0.04))

        cls.sobol_out = sensitivity.run_expense_sensitivity_analysis(cls.model_in, cls.params)
        cls.morris_out = sensitivity.run_expense_ranking_analysis(cls.model_in, cls.params)

    @unittest.skipIf(not getenv('TEST_SLOW', False), 'Slow test')
    def test_expenses_boxplot(self):
        box_plot_svg = expenses_boxplot(self.sobol_out.runs)

        self.assertTrue(box_plot_svg.startswith('<img src="data:image/svg+xml;base64,'))
        base64_content = box_plot_svg[len('<img src="data:image/svg+xml;base64,'):-3]  # for "/> end

        # will raise exception if base64 incorrectly encoded or XML is invalid; does not validate against schema
        svg_content = base64.b64decode(base64_content, validate=True)
        _ = ET.fromstring(svg_content)

    @unittest.skipIf(not getenv('TEST_SLOW', False), 'Slow test')
    def test_render_report(self):
        report = render_report(self.model_in, self.params, self.sobol_out, self.morris_out)
        with open('testreport.html', 'wt') as out:
            out.write(report)
