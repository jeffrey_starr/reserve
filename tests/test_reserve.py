from math import floor
import numpy as np
import unittest
import reserve


class ComponentTest(unittest.TestCase):

    def test_ffb(self):
        components = [
            reserve.Component('Pool Furniture - Replace', ul=5, rul=0, cost=4_600),
            reserve.Component('Pool Resurface', ul=10, rul=5, cost=10_000),
            reserve.Component('Roof Replace', ul=20, rul=18, cost=80_000),
            reserve.Component('Asphalt Seal', ul=5, rul=2, cost=5_000),
            reserve.Component('Asphalt Resurface', ul=20, rul=2, cost=25_000),
            reserve.Component('Building Repaint', ul=10, rul=1, cost=50_000),
            reserve.Component('Elevator Modernize', ul=20, rul=5, cost=80_000),
            reserve.Component('Hallways Refurbish', ul=8, rul=6, cost=24_000)
        ]
        ffbs = [4_600, 5_000, 8_000, 3_000, 22_500, 45_000, 60_000, 6_000]
        for c, ffb in zip(components, ffbs):
            self.assertAlmostEqual(c.ffb(), ffb)

    def test_ffb_projection(self):
        c = reserve.Component('Asphalt Seal', ul=5, rul=2, cost=5_000)
        self.assertAlmostEqual(3_000, c.ffb_projection(0, 0))  # eff_age = 3, rul = 2
        self.assertAlmostEqual(4_000, c.ffb_projection(1, 0))  # eff_age = 4, rul = 1
        self.assertAlmostEqual(5_000, c.ffb_projection(2, 0))  # eff_age = 5, rul = 0
        self.assertAlmostEqual(1_000, c.ffb_projection(3, 0))  # eff_age = 1, rul = 4
        self.assertAlmostEqual(2_000, c.ffb_projection(4, 0))  # eff_age = 2, rul = 3
        self.assertAlmostEqual(3_000, c.ffb_projection(5, 0))  # eff_age = 3, rul = 2
        self.assertAlmostEqual(4_000, c.ffb_projection(6, 0))  # eff_age = 4, rul = 1
        self.assertAlmostEqual(5_000, c.ffb_projection(7, 0))  # eff_age = 5, rul = 0
        self.assertAlmostEqual(1_000, c.ffb_projection(8, 0))  # eff_age = 1, rul = 4

    def test_replacement_years_single_replacement(self):
        for rul in range(0, reserve.YEARS):
            component = reserve.Component('name', reserve.YEARS, rul, 0.0)
            replacements_at = component.replacement_years()
            self.assertEqual(reserve.YEARS, len(replacements_at))
            self.assertEqual(1, replacements_at[rul])
            self.assertEqual(1, replacements_at.sum())

    def test_replacement_years_useful_life_5(self):
        component = reserve.Component('name', ul=5, rul=0, cost=0.0)
        replacements_at = component.replacement_years()
        for y in [0, 5, 10, 15, 20, 25]:
            self.assertEqual(1, replacements_at[y])
        self.assertEqual(6, replacements_at.sum())

        component = reserve.Component('name', ul=5, rul=4, cost=0.0)
        replacements_at = component.replacement_years()
        for y in [4, 9, 14, 19, 24, 29]:
            self.assertEqual(1, replacements_at[y])
        self.assertEqual(6, replacements_at.sum())

        component = reserve.Component('name', ul=5, rul=2, cost=0.0)
        replacements_at = component.replacement_years()
        for y in [2, 7, 12, 17, 22, 27]:
            self.assertEqual(1, replacements_at[y])
        self.assertEqual(6, replacements_at.sum())


class ExpensesInflationAdj(unittest.TestCase):

    expenses = np.swapaxes(np.array([[100.0, 100.0, 100.0, 100.0]]), 0, 1)

    def test_zero_no_change(self):
        inflated = reserve._expenses_inflation_adj(self.expenses, 0.0)
        self.assertTrue(np.all(np.equal(self.expenses, inflated)))

    def test_two_percent_inflation(self):
        inflated = reserve._expenses_inflation_adj(self.expenses, 0.02)
        self.assertAlmostEqual(self.expenses[0][0], inflated[0][0])  # no inflation applied to first year
        self.assertAlmostEqual(102.0, inflated[1][0])
        self.assertAlmostEqual(104.04, inflated[2][0])
        self.assertAlmostEqual(106.1208, inflated[3][0])


class MIn(unittest.TestCase):
    components = [
        reserve.Component('Pool Furniture - Replace', ul=5, rul=0, cost=4_600),
        reserve.Component('Pool Resurface', ul=10, rul=5, cost=10_000),
        reserve.Component('Roof Replace', ul=20, rul=18, cost=80_000),
        reserve.Component('Asphalt Seal', ul=5, rul=2, cost=5_000),
        reserve.Component('Asphalt Resurface', ul=20, rul=2, cost=25_000),
        reserve.Component('Building Repaint', ul=10, rul=1, cost=50_000),
        reserve.Component('Elevator Modernize', ul=20, rul=5, cost=80_000),
        reserve.Component('Hallways Refurbish', ul=8, rul=6, cost=24_000)
    ]
    min = reserve.MIn(components, starting_balance=0)

    def test_ffb(self):
        ffb = self.min.ffb()
        self.assertAlmostEqual(154_100, ffb)

    def test_ffbs(self):
        inflation_rate = 0.03
        ffb_1 = self.min.ffb_projection(1, inflation_rate)
        self.assertAlmostEqual(174_760, floor(ffb_1))  # 'Calculating Reserve Funding Plans' has 174_761


class MInParam(unittest.TestCase):
    def test_reserve_contrib_norm_float(self):
        param = reserve.MInParam(100, 0.03, 0.0)
        contribs = param.reserve_contrib_norm()
        self.assertEqual(reserve.YEARS, len(contribs))
        self.assertAlmostEqual(100, contribs[0])
        self.assertAlmostEqual(103.00, contribs[1])
        self.assertAlmostEqual(106.09, contribs[2])
        self.assertAlmostEqual(235.66, contribs[29], 2)

    def test_reserve_contrib_norm_array(self):
        param = reserve.MInParam(np.zeros(reserve.YEARS), 0.03, 0.0)
        contribs = param.reserve_contrib_norm()
        self.assertEqual(0, contribs.sum())
