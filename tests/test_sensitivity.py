from os import getenv
import numpy as np
import result

import reserve
from reserve import sensitivity
import unittest


components = [
    sensitivity.UncertainComponent('Pool Furniture - Replace', ul=5, rul=(0, 0), cost=(4_500, 4_700)),
    sensitivity.UncertainComponent('Pool Resurface', ul=10, rul=(4, 7), cost=(9_000, 11_000)),
    sensitivity.UncertainComponent('Roof Replace', ul=20, rul=(16, 20), cost=(60_000, 100_000)),
    sensitivity.UncertainComponent('Asphalt Seal', ul=5, rul=(1, 3), cost=(4_500, 5_500)),
    sensitivity.UncertainComponent('Asphalt Resurface', ul=20, rul=(1, 3), cost=(20_000, 30_000)),
    sensitivity.UncertainComponent('Building Repaint', ul=10, rul=(1, 2), cost=(40_000, 60_000)),
    sensitivity.UncertainComponent('Elevator Modernize', ul=20, rul=(3, 7), cost=(70_000, 90_000)),
    sensitivity.UncertainComponent('Hallways Refurbish', ul=8, rul=(5, 7), cost=(22_000, 26_000))
]

model_in = sensitivity.UncertainMIn(components)
params = sensitivity.UncertainMInParam(inflation_rate=(0.02, 0.04))


class ExpenseSensitivity(unittest.TestCase):

    @unittest.skipIf(not getenv('TEST_SLOW', False), 'Slow test')
    def test_check_execution(self):
        sobol_out = sensitivity.run_expense_sensitivity_analysis(model_in, params)
        # this is a low-quality test to ensure that data is generated.
        self.assertIsInstance(sobol_out.sobol, dict)

    @unittest.skipIf(not getenv('TEST_SLOW', False), 'Slow test')
    def test_morris_execution(self):
        morris_out = sensitivity.run_expense_ranking_analysis(model_in, params)
        self.assertIsInstance(morris_out.morris, dict)

    def test_problem(self):
        problem = sensitivity._problem(model_in, params)
        self.assertEqual(1 + 2 * 8, problem['num_vars'])

        self.assertEqual('r', problem['names'][0])
        self.assertEqual((0.02, 0.04), problem['bounds'][0])

        self.assertEqual('Pool Furniture - Replace_rul', problem['names'][1])
        self.assertEqual('Pool Furniture - Replace_cost', problem['names'][2])
        self.assertEqual(0, problem['bounds'][1][0])
        self.assertAlmostEqual(0, problem['bounds'][1][1])
        self.assertEqual(4_500, problem['bounds'][2][0])
        self.assertEqual(4_700, problem['bounds'][2][1])

        self.assertEqual('Hallways Refurbish_rul', problem['names'][15])
        self.assertEqual('Hallways Refurbish_cost', problem['names'][16])
        self.assertEqual(5, problem['bounds'][15][0])
        self.assertEqual(7, problem['bounds'][15][1])
        self.assertEqual(22_000, problem['bounds'][16][0])
        self.assertEqual(26_000, problem['bounds'][16][1])

    def test_evaluate_expense_model(self):
        components = [sensitivity.UncertainComponent('Flat', ul=5, rul=(1,2), cost=(900, 1_100))]
        model_in = sensitivity.UncertainMIn(components)

        X = np.array([0.00, 1.0, 1_000.0])  # no inflation, replacements at 1, 6, 11, 16, 21, and 26, cost $1k
        mout = sensitivity._evaluate_expense_model(model_in, X)

        self.assertAlmostEqual(6_000, mout.expense_by_year.sum())

    def test_bounds(self):
        self.assertEqual((1, 1.000_000_001), sensitivity._bounds((1, 1)))
        self.assertEqual((23.4, 45.7), sensitivity._bounds((23.4, 45.7)))


class CSVImport(unittest.TestCase):

    def test_import_valid_file(self):
        with open('data/basic_model_cost_20pct.csv', mode='rt', newline='') as f:
            datum = f.read()

        maybe_model = sensitivity.components_from_csv(datum)
        self.assertIsInstance(maybe_model, result.Ok)
        model = maybe_model.ok()

        self.assertEqual(8, len(model))

        self.assertEqual(sensitivity.UncertainComponent('Pool - Resurface', 10, rul=(5, 5), cost=(8000.0, 12000.0)),
                         model[1])


class UncertainComponent(unittest.TestCase):
    def test_to_certain(self):
        c = sensitivity.UncertainComponent('Asphalt Resurface', ul=20, rul=(1, 3), cost=(20_000, 30_000))
        self.assertEqual(reserve.Component(c.name, c.ul, 2, 25_000), c.to_certain('ml'))
        self.assertEqual(reserve.Component(c.name, c.ul, 3, 20_000), c.to_certain('best'))
        self.assertEqual(reserve.Component(c.name, c.ul, 1, 30_000), c.to_certain('worst'))
        self.assertRaises(ValueError, lambda: c.to_certain('undefined_case'))


class FFBCumDistFunction(unittest.TestCase):
    def test_too_large(self):
        saved_limit = sensitivity.MAX_COMPONENTS_FOR_CDF
        sensitivity.MAX_COMPONENTS_FOR_CDF = 2

        try:
            comps = [reserve.Component('n', 1, 0, 1.0),
                     reserve.Component('p', 1, 0, 1.0),
                     reserve.Component('q', 1, 0, 1.0)]
            r = sensitivity.ffb_cdf(comps)
            self.assertEqual(result.Err('Too many components for FFB CDF analysis. Saw 3, limit is 2.'), r)
        finally:
            sensitivity.MAX_COMPONENTS_FOR_CDF = saved_limit

    def test_empty_components_list(self):
        r = sensitivity.ffb_cdf([]).ok()

        self.assertEqual((1, 2), r.shape)
        self.assertAlmostEqual(1.0, r[0][0])
        self.assertAlmostEqual(0.0, r[0][1])

    def test_three(self):
        comps = [
            reserve.Component('c1', 10, 9, 5.0),
            reserve.Component('c2', 2, 1, 3.0),
            reserve.Component('c3', 10, 2, 7.0)
        ]

        self.assertAlmostEqual(0.1, comps[0].effective_percent_age())
        self.assertAlmostEqual(0.5, comps[1].effective_percent_age())
        self.assertAlmostEqual(0.8, comps[2].effective_percent_age())

        r = sensitivity.ffb_cdf(comps).ok()

        # expected values
        # 2.52  0.36
        # 6.12  0.72
        # 6.12  0.81
        # 6.39  0.9
        # 6.87  0.94
        # 7.47  0.98
        # 7.52  0.99
        # 7.6   1

        self.assertAlmostEqual(2.52, r[0][0])
        self.assertAlmostEqual(0.36, r[0][1])
        self.assertAlmostEqual(6.39, r[3][0])
        self.assertAlmostEqual(0.9, r[3][1])
        self.assertAlmostEqual(7.6, r[7][0])
        self.assertAlmostEqual(1, r[7][1])
