import unittest
import reserve


# The basic models follows the article 'Calculating Reserve Funding Plans' in the CAI's
# *An Explanation of Reserve Study Standards* dated 2019-11. A copy can be downloaded from
# https://www.caionline.org/LearningCenter/credentials/Documents/NRSSClarificationArticles.pdf

components = [
    reserve.Component('Pool Furniture - Replace', ul=5, rul=0, cost=4_600),
    reserve.Component('Pool Resurface', ul=10, rul=5, cost=10_000),
    reserve.Component('Roof Replace', ul=20, rul=18, cost=80_000),
    reserve.Component('Asphalt Seal', ul=5, rul=2, cost=5_000),
    reserve.Component('Asphalt Resurface', ul=20, rul=2, cost=25_000),
    reserve.Component('Building Repaint', ul=10, rul=1, cost=50_000),
    reserve.Component('Elevator Modernize', ul=20, rul=5, cost=80_000),
    reserve.Component('Hallways Refurbish', ul=8, rul=6, cost=24_000)
]

model_in = reserve.MIn(components, starting_balance=76_450)
model_params = reserve.MInParam(reserve_contrib=21_500, inflation_rate=0.0, interest_rate=0.0)

# Model only demonstrated for 8 years
annual_expenses = [4_600, 50_000, 30_000, 0, 0, 94_600, 24_000, 5_000]
ending_balance = [93_350, 64_850, 56_350, 77_850, 99_350, 26_250, 23_750, 40_250]


class ModelConformance(unittest.TestCase):


    def test_annual_expenses(self):
        mout = reserve.run_simulation(model_in, model_params)
        self.assertAlmostEqual(annual_expenses[0], mout.expense_by_year[0])
        self.assertAlmostEqual(annual_expenses[1], mout.expense_by_year[1])
        self.assertAlmostEqual(annual_expenses[2], mout.expense_by_year[2])
        self.assertAlmostEqual(annual_expenses[3], mout.expense_by_year[3])
        self.assertAlmostEqual(annual_expenses[4], mout.expense_by_year[4])
        self.assertAlmostEqual(annual_expenses[5], mout.expense_by_year[5])
        self.assertAlmostEqual(annual_expenses[6], mout.expense_by_year[6])
        self.assertAlmostEqual(annual_expenses[7], mout.expense_by_year[7])

        self.assertAlmostEqual(ending_balance[0], mout.end_balance_by_year[0])
        self.assertAlmostEqual(ending_balance[1], mout.end_balance_by_year[1])
        self.assertAlmostEqual(ending_balance[2], mout.end_balance_by_year[2])
        self.assertAlmostEqual(ending_balance[3], mout.end_balance_by_year[3])
        self.assertAlmostEqual(ending_balance[4], mout.end_balance_by_year[4])
        self.assertAlmostEqual(ending_balance[5], mout.end_balance_by_year[5])
        self.assertAlmostEqual(ending_balance[6], mout.end_balance_by_year[6])
        self.assertAlmostEqual(ending_balance[7], mout.end_balance_by_year[7])
